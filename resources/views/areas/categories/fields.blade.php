@php(
    $langs = Trans::languages()
)
<ul class="nav nav-tabs" role="tablist">
    @foreach($langs as $lang)
        <li role="presentation" class="{!! $lang['locale'] == Trans::locale() ? 'active' : '' !!}"><a href="{!! "#tab{$lang['id']}{$lang['locale']}" !!}" aria-controls="{!! "tab{$lang['id']}{$lang['locale']}" !!}" role="tab" data-toggle="tab">{!! $lang['title'] !!}</a></li>
    @endforeach
</ul>
<div class="tab-content">
    @foreach($langs as $lang)
        <div role="tabpanel" class="{!! $lang['locale'] == Trans::locale() ? 'tab-pane active' : 'tab-pane' !!}" id="{!! "tab{$lang['id']}{$lang['locale']}" !!}">
            <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">{{ trans('labelAdmin.areasCategory.title') }}<span class="text-danger">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" class="form-control" id="title" name="title" placeholder="{{ trans('labelAdmin.areasCategory.placeholderTitle') }}"
                           value="{!! (!empty($data)) ? $data->title : old('title') !!}">
                </div>
            </div>
        </div>

    @endforeach
            <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">{{ trans('labelAdmin.areasCategory.status') }}<span class="text-danger">*</span></label>
                <div class="col-md-9 col-sm-9 col-xs-12 m-t-5">
                    <input type="checkbox" name="active" class="js-switch" @if((!empty($data) && $data->active) || old('active')) {!! "checked" !!} @endif>
                </div>
            </div>
</div>