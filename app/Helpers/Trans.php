<?php
/**
 * Created by PhpStorm.
 * User: ductm
 * Date: 7/3/2018
 * Time: 5:16 PM
 */

namespace App\Helpers;

use App\Languages;
use Illuminate\Support\Facades\App;

class Trans
{
    protected $locale;

    public function __construct()
    {
        $this->locale = App::getLocale();
    }

    /*
     *  Get locale
     *  @return string
     */
    public function locale() {
        return $this->locale;
    }

    public function languages() {
        $languages = Languages::where('active',1)->get();
        return $languages;
    }
}