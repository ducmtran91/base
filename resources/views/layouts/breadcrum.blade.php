<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        {!! $breadcrum !!}
    </ol>
</nav>