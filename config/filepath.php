<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | File path
    |--------------------------------------------------------------------------
    |
    | Intervention Image supports "GD Library" and "Imagick" to process images
    | internally. You may choose one of them according to your PHP
    | configuration. By default PHP's "GD Library" implementation is used.
    |
    | Supported: "gd", "imagick"
    |
    */

    'no_image' => '/themes/no-image.png',

    'language_image' => public_path('uploads/languages/images'),
    'language_image_show' => '/uploads/languages/images/',
);