@extends('layouts.app')

@section('content')
    <form action="{!! action('AreaCategoryController@update',$id) !!}" method="post" class="form-horizontal form-label-left">
        {{ csrf_field() }}
        <input name="_method" value="PUT" type="hidden"/>
        @include('areas.categories.fields')
        <button type="submit" class="btn btn-primary">{{ trans('button.update') }}</button>
    </form>
@endsection