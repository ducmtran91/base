<?php
/**
 * Created by PhpStorm.
 * User: ductm
 * Date: 6/29/2018
 * Time: 2:03 PM
 */

namespace App\Http\Controllers;


use Illuminate\Support\Facades\View;

class AppBaseController extends Controller
{
    protected $perPage;
    protected $model;
    protected $breadcrum;

    public function __construct()
    {
        $this->perPage = 10;
        $this->model = null;
        $this->breadcrum = '';
        $this->middleware('auth');
        View::share('status', config('constant.status'));
        View::share('limit', config('constant.limit'));
    }
}