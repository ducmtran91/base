<?php
/**
 * Created by PhpStorm.
 * User: ductm
 * Date: 7/2/2018
 * Time: 9:09 AM
 */

namespace App\Repositories\Criteria\Generate;


use App\Repositories\Criteria\Criteria;
use App\Repositories\Eloquent\Repository;

class Keyword extends Criteria
{
    private $keyword;
    public function __construct($keyword)
    {
        $this->keyword = $keyword;
    }

    public function apply($model, Repository $repository)
    {
        $model = $model->where('title','like',"%{$this->keyword}%");
        return $model;
    }
}