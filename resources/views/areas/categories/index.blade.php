@extends('layouts.app')

@section('content')
    <div id="datatable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
        @include('areas.categories.action')
        <form method="get" action="" id="form_custom_list">
            <div>
                <div class="input-group m-b-0">
                    <span class="input-group-addon"><i class="fa fa-search"></i></span>
                    <input type="text" class="form-control" name="filter_keyword" placeholder="{{ trans('labelAdmin.areasCategory.placeholderSearch') }}..." value="{{ $queryParams['filter_keyword'] }}">
                </div>
                <select name="filter_status" class="form-control filter_header">
                    <option value="none" {!! ($queryParams['filter_status'] == 'none') ? 'selected' : '' !!}>Select option</option>
                    {{--@foreach($status as $key => $value)--}}
                        {{--<option value="{{ $key }}" {!! ($queryParams['filter_status'] == $key) ? 'selected' : '' !!}>{{ $value }}</option>--}}
                    {{--@endforeach--}}
                    <option value="1" {!! ($queryParams['filter_status'] == '1') ? 'selected="selected"' : '' !!}>Hoạt động
                    </option>
                    <option value="0" {!! ($queryParams['filter_status'] == '0') ? 'selected="selected"' : '' !!}>Không hoạt
                        động
                    </option>
                </select>
            </div>
            <div class="navbar-right">
                <div class="input-group">
                    <select name="filter_limit" aria-controls="datatable" class="form-control input-sm filter_header">
                        <option value="10" {!! ($queryParams['filter_limit'] == 10) ? 'selected' : '' !!}>10</option>
                        <option value="25" {!! ($queryParams['filter_limit'] == 25) ? 'selected' : '' !!}>25</option>
                        <option value="50" {!! ($queryParams['filter_limit'] == 50) ? 'selected' : '' !!}>50</option>
                        <option value="100" {!! ($queryParams['filter_limit'] == 100) ? 'selected' : '' !!}>100</option>
                    </select>
                </div>
            </div>
        </form>

        <div class="row">
            <div class="col-sm-12">
                <table id="datatable" class="table table-striped table-bordered dataTable no-footer" role="grid"
                       aria-describedby="datatable_info">
                    <thead>
                    <tr role="row">
                        <th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1"
                            aria-label="Name: activate to sort column ascending" style="width: 263px;">#
                        </th>
                        <th class="sorting_desc" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1"
                            aria-label="Position: activate to sort column ascending" aria-sort="descending"
                            style="width: 420px;">{{ trans('labelAdmin.areasCategory.title') }}
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1"
                            aria-label="Office: activate to sort column ascending" style="width: 201px;">{{ trans('labelAdmin.areasCategory.status') }}
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1"
                            aria-label="Age: activate to sort column ascending" style="width: 113px;">{{ trans('labelAdmin.areasCategory.action') }}
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @if($datas)
                        @foreach($datas as $item)
                            <tr role="row" class="even">
                                <th scope="row">{!! $item->id !!}</th>
                                <td>{!! $item->title !!}</td>
                                <td>{!! $status[$item->active] !!}</td>
                                <td>
                                    <a href="{!! route('area.category.show', ['id' => $item->id]) !!}" class="btn btn-primary btn-xs"><i class="fa fa-folder"></i></a>
                                    <a href="{!! route('area.category.edit', ['id' => $item->id]) !!}" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i></a>
                                    <form id="delete" action="{!! route('area.category.destroy', ['id' => $item->id]) !!}"
                                          method="POST">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-7">
                <div class="dataTables_paginate paging_simple_numbers" id="datatable_paginate">
                    {{ $datas->appends($queryParams)->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection