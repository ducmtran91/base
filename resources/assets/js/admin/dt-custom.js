DT = {};
DT.fn = {
    init: function() {
        DT.fn.initFilter();
    },
    initFilter: function() {
        $('.filter_header').change(function() {
            $('#form_custom_list').submit();
        });
    },
    rule: function(){
        jQuery(document).ready(function($){
            DT.fn.init.call();
        });
    }
};
DT.fn.rule();