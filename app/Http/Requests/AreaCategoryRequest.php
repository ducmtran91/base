<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AreaCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method())
        {
            case 'POST':
                return [
                    'title' => 'required|unique:area_categories|max:255'
                ];
                break;
            case 'PUT':
                return [
                    'title' => 'required|max:255|unique:area_categories,title,' .$this->route()->parameter('id'),
                ];
                break;
        }
    }
}
