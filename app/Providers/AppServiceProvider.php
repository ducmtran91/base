<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
            \DB::listen(function ($query) {
            $test = var_export([
                $query->sql,
                $query->bindings,
                // $query->time
            ], true);

            file_put_contents('custom_query_log.txt', $test . PHP_EOL . '-------' . PHP_EOL, FILE_APPEND);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
