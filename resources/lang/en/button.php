<?php
return [
    'add' => 'Add New',
    'update' => 'Update',
    'submit' => 'Submit',
    'importCSV' => 'Import CSV',
    'import' => 'Import',
    'close' => 'Close',
];