<?php
/**
 * Created by PhpStorm.
 * User: ductm
 * Date: 6/15/2018
 * Time: 4:37 PM
 */

namespace App\Repositories\Models;


use App\AreaCategory;
use App\Repositories\Eloquent\Repository;

class AreaCategoryRepository extends Repository
{
    public function model()
    {
        return AreaCategory::class;
    }
}