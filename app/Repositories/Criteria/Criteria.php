<?php
/**
 * Created by PhpStorm.
 * User: ductm
 * Date: 6/15/2018
 * Time: 5:21 PM
 */

namespace App\Repositories\Criteria;


use App\Repositories\Eloquent\Repository;

/**an abstract non instantiable class**/
abstract class Criteria
{
    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public abstract function apply($model, Repository $repository);
    /*This method will hold criteria query which will be applied in the Repository class on the concrete entity*/
}