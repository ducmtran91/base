<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AreaCategory extends Model
{
    protected $fillable = [
        'title',
        'active'
    ];
}
