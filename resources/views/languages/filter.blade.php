{!! Form::open(['method' => 'GET', 'id' => 'form_custom_list']) !!}
    <div class="input-group m-b-0">
        <span class="input-group-addon"><i class="fa fa-search"></i></span>
        {!! Form::text('filter_keyword', $queryParams['filter_keyword'], ['class' => 'form-control', 'placeholder' => trans('labelAdmin.areasCategory.placeholderSearch')]) !!}
    </div>
    {!! Form::select('filter_status', array_merge(['' => 'Select option'], $status), $queryParams['filter_status'], ['class' => 'form-control filter_header']) !!}
    <div class="navbar-right">
        <div class="input-group">
            {!! Form::select('filter_limit', $limit, $queryParams['filter_limit'], ['class' => 'form-control input-sm filter_header']) !!}
        </div>
    </div>
{!! Form::close() !!}