var elixir = require('laravel-elixir');
// elixir.config.sourcemaps = false;
elixir(function(mix){
    // mix.copy('bower_components/gentelella/vendors/bootstrap/dist/css/bootstrap.min.css', 'resources/assets/css/admin/bootstrap.min.css');
    // mix.copy('bower_components/gentelella/vendors/font-awesome/css/font-awesome.min.css', 'resources/assets/css/admin/font-awesome.min.css');
    // mix.copy('bower_components/gentelella/vendors/nprogress/nprogress.css', 'resources/assets/css/admin/nprogress.css');
    // mix.copy('bower_components/gentelella/vendors/iCheck/skins/flat/green.css', 'resources/assets/css/admin/green.css');
    // mix.copy('bower_components/gentelella/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css', 'resources/assets/css/admin/bootstrap-progressbar-3.3.4.min.css');
    // mix.copy('bower_components/gentelella/vendors/jqvmap/dist/jqvmap.min.css', 'resources/assets/css/admin/jqvmap.min.css');
    // mix.copy('bower_components/gentelella/vendors/bootstrap-daterangepicker/daterangepicker.css', 'resources/assets/css/admin/daterangepicker.css');
    // mix.copy('bower_components/gentelella/vendors/switchery/dist/switchery.min.css', 'resources/assets/css/admin/switchery.css');
    // mix.copy('bower_components/gentelella/vendors/select2/dist/css/select2.min.css', 'resources/assets/css/admin/select2.css');
    // mix.copy('bower_components/gentelella/vendors/google-code-prettify/bin/prettify.min.css', 'resources/assets/css/admin/prettify.css');
    // mix.copy('bower_components/gentelella/build/css/custom.min.css', 'resources/assets/css/admin/custom.min.css');
    mix.styles([
        'resources/assets/css/admin/bootstrap.min.css',
        'resources/assets/css/admin/font-awesome.min.css',
        'resources/assets/css/admin/nprogress.css',
        'resources/assets/css/admin/green.css',
        'resources/assets/css/admin/bootstrap-progressbar-3.3.4.min.css',
        'resources/assets/css/admin/jqvmap.min.css',
        'resources/assets/css/admin/prettify.css',
        'resources/assets/css/admin/select2.css',
        'resources/assets/css/admin/switchery.css',
        'resources/assets/css/admin/daterangepicker.css',
        'resources/assets/css/admin/custom.min.css',
        'resources/assets/css/admin/style.css'
    ],'public/css/admin.css');

    //The third argument to both the styles and scripts methods determines the relative directory for all paths passed to the methods.
    // mix.styles([
    //     "normalize.css",
    //     "main.css"
    // ], 'resources/assets/build/css/everything.css', 'resources/assets/css');
    // mix.copy('bower_components/gentelella/vendors/jquery/dist/jquery.min.js', 'resources/assets/js/admin/jquery.min.js');
    // mix.copy('bower_components/gentelella/vendors/bootstrap/dist/js/bootstrap.min.js', 'resources/assets/js/admin/bootstrap.min.js');
    // mix.copy('bower_components/gentelella/vendors/fastclick/lib/fastclick.js', 'resources/assets/js/admin/fastclick.js');
    // mix.copy('bower_components/gentelella/vendors/nprogress/nprogress.js', 'resources/assets/js/admin/nprogress.js');
    // mix.copy('bower_components/gentelella/vendors/Chart.js/dist/Chart.min.js', 'resources/assets/js/admin/Chart.min.js');
    // mix.copy('bower_components/gentelella/vendors/gauge.js/dist/gauge.min.js', 'resources/assets/js/admin/gauge.min.js');
    // mix.copy('bower_components/gentelella/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js', 'resources/assets/js/admin/bootstrap-progressbar.min.js');
    // mix.copy('bower_components/gentelella/vendors/iCheck/icheck.min.js', 'resources/assets/js/admin/icheck.min.js');
    //
    // mix.copy('bower_components/gentelella/vendors/skycons/skycons.js', 'resources/assets/js/admin/skycons.js');
    // mix.copy('bower_components/gentelella/vendors/Flot/jquery.flot.js', 'resources/assets/js/admin/jquery.flot.js');
    // mix.copy('bower_components/gentelella/vendors/Flot/jquery.flot.pie.js', 'resources/assets/js/admin/jquery.flot.pie.js');
    // mix.copy('bower_components/gentelella/vendors/Flot/jquery.flot.time.js', 'resources/assets/js/admin/jquery.flot.time.js');
    // mix.copy('bower_components/gentelella/vendors/Flot/jquery.flot.stack.js', 'resources/assets/js/admin/jquery.flot.stack.js');
    // mix.copy('bower_components/gentelella/vendors/Flot/jquery.flot.resize.js', 'resources/assets/js/admin/jquery.flot.resize.js');
    // mix.copy('bower_components/gentelella/vendors/flot.orderbars/js/jquery.flot.orderBars.js', 'resources/assets/js/admin/jquery.flot.orderBars.js');
    // mix.copy('bower_components/gentelella/vendors/flot-spline/js/jquery.flot.spline.min.js', 'resources/assets/js/admin/jquery.flot.spline.min.js');
    //
    // mix.copy('bower_components/gentelella/vendors/flot.curvedlines/curvedLines.js', 'resources/assets/js/admin/curvedLines.js');
    // mix.copy('bower_components/gentelella/vendors/DateJS/build/date.js', 'resources/assets/js/admin/date.js');
    // mix.copy('bower_components/gentelella/vendors/jqvmap/dist/jquery.vmap.js', 'resources/assets/js/admin/jquery.vmap.js');
    // mix.copy('bower_components/gentelella/vendors/jqvmap/dist/maps/jquery.vmap.world.js', 'resources/assets/js/admin/jquery.vmap.world.js');
    // mix.copy('bower_components/gentelella/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js', 'resources/assets/js/admin/jquery.vmap.sampledata.js');
    // mix.copy('bower_components/gentelella/vendors/moment/min/moment.min.js', 'resources/assets/js/admin/moment.min.js');
    // mix.copy('bower_components/gentelella/vendors/bootstrap-daterangepicker/daterangepicker.js', 'resources/assets/js/admin/daterangepicker.js');
    // mix.copy('bower_components/gentelella/vendors/switchery/dist/switchery.min.js', 'resources/assets/js/admin/switchery.js');
    // mix.copy('bower_components/gentelella/vendors/select2/dist/js/select2.full.min.js', 'resources/assets/js/admin/select2.js');
    // mix.copy('bower_components/gentelella/vendors/google-code-prettify/bin/prettify.min.js', 'resources/assets/js/admin/prettify.js');
    // mix.copy('bower_components/gentelella/build/js/custom.min.js', 'resources/assets/js/admin/custom.min.js');
    mix.scripts([
        'resources/assets/js/admin/jquery.min.js',
        'resources/assets/js/admin/switchery.js',
        'resources/assets/js/admin/bootstrap.min.js',
        'resources/assets/js/admin/fastclick.js',
        'resources/assets/js/admin/nprogress.js',
        'resources/assets/js/admin/Chart.min.js',
        'resources/assets/js/admin/gauge.min.js',
        'resources/assets/js/admin/icheck.min.js',
        'resources/assets/js/admin/bootstrap-progressbar.min.js',
        'resources/assets/js/admin/skycons.js',
        'resources/assets/js/admin/jquery.flot.js',
        'resources/assets/js/admin/jquery.flot.pie.js',
        'resources/assets/js/admin/jquery.flot.time.js',
        'resources/assets/js/admin/jquery.flot.stack.js',
        'resources/assets/js/admin/jquery.flot.resize.js',
        'resources/assets/js/admin/jquery.flot.orderBars.js',
        'resources/assets/js/admin/jquery.flot.spline.min.js',
        'resources/assets/js/admin/curvedLines.js',
        'resources/assets/js/admin/date.js',
        'resources/assets/js/admin/jquery.vmap.js',
        'resources/assets/js/admin/jquery.vmap.world.js',
        'resources/assets/js/admin/jquery.vmap.sampledata.js',
        'resources/assets/js/admin/daterangepicker.js',
        'resources/assets/js/admin/moment.min.js',
        'resources/assets/js/admin/prettify.js',
        'resources/assets/js/admin/select2.js',
        'resources/assets/js/admin/custom.min.js',
        'resources/assets/js/admin/dt-custom.js',
    ],'public/js/admin.js');

    //Combine All Scripts in a Directory
    // mix.scriptsIn("resources/assets/js");
})