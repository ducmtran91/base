@extends('layouts.app')

@section('content')
    <div class="jumbotron">
        @if($data)
            <table>
                <tr>
                    <td>{{ trans('labelAdmin.areasCategory.title') }}: </td>
                    <td>{!! $data->title !!}</td>
                </tr>
                <tr>
                    <td>{{ trans('labelAdmin.areasCategory.status') }}: </td>
                    <td>{!! $status[$data->active] !!}</td>
                </tr>
                <tr>
                    <td>{{ trans('labelAdmin.areasCategory.createdAt') }}: </td>
                    <td>{!! $data->created_at !!}</td>
                </tr>
            </table>
        @else
            <p>{{ trans('labelAdmin.notData') }}</p>
        @endif
    </div>
@endsection