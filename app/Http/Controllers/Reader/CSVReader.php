<?php
/**
 * Created by PhpStorm.
 * User: ductm
 * Date: 7/12/2018
 * Time: 9:39 AM
 */

namespace App\Http\Controllers\Reader;
use Maatwebsite\Excel\Facades\Excel;

class CSVReader extends FileReader
{
    private $file;
    private $nameField;
    public function __construct($file, $nameField)
    {
        $this->file = $file;
        $this->nameField = $nameField;
    }

    protected function getPathFile()
    {
        return $this->file->file($this->nameField)->getRealPath();
    }

    public function parseData()
    {
        $path = $this->getPathFile();//self: only static
        if($this->file->has('header')) {
            $data = Excel::load($path, function ($reader){})->get()->toArray();
        }else{
            $dataFile = file($path);
            $data = [];
            foreach($dataFile as $item){
                $item = str_getcsv($item);
                if(array_filter($item)){
                    $data[] = $item;
                }
            }
        }
        return $data;
    }

    public function header($data)
    {
        return $data[0];
    }
}