<?php
/**
 * Created by PhpStorm.
 * User: ductm
 * Date: 7/12/2018
 * Time: 12:14 PM
 */

namespace App\Repositories\Criteria\Generate;


use App\Repositories\Criteria\Criteria;
use App\Repositories\Eloquent\Repository;

class Locale extends Criteria
{
    private $locale;
    private $operation;
    public function __construct($locale, $operation = '=')
    {
        $this->locale = $locale;
        $this->operation = $operation;
    }

    public function apply($model, Repository $repository)
    {
        $model = $model->where('locale', $this->operation, $this->locale);
        return $model;
    }
}