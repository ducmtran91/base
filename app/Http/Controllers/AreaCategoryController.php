<?php
/**
 * Created by PhpStorm.
 * User: ductm
 * Date: 6/11/2018
 * Time: 5:58 PM
 */

namespace App\Http\Controllers;

use App\Http\Requests\AreaCategoryRequest;
use App\Repositories\Criteria\Generate\Keyword;
use App\Repositories\Criteria\Generate\Status;
use App\Repositories\Models\AreaCategoryRepository;
use Illuminate\Http\Request;

class AreaCategoryController extends AppBaseController
{
    private $modelController;

    public function __construct(AreaCategoryRepository $model)
    {
        parent::__construct();
        $this->modelController = $model;
        $this->breadcrum = "<li class='breadcrumb-item'><a href='".route('area.category')."'><i class='fa fa-home'></i>  ".trans('labelAdmin.areasCategory.listTitle')."</a></li>";
    }

    public function index(Request $request) {
        $limit = empty($request->get('filter_limit')) ? $this->perPage : $request->get('filter_limit');
        $status = empty($request->get('filter_status')) ? 'none' : $request->get('filter_status');
        $keyword = empty($request->get('filter_keyword')) ? '' : $request->get('filter_keyword');
        $model = $this->modelController;
        if($keyword || $status != "none"){
            if($keyword) {
                $model = $model->pushCriteria(new Keyword($keyword));
            }
            if($status != "none") {
                $model = $model->pushCriteria(new Status($status));
            }
            $datas = $model->paginate($limit);
        }else{
            $datas = $model->paginate($limit);
        }
        $queryParams = [
            'filter_limit' => $limit,
            'filter_status' => $status,
            'filter_keyword' => $keyword
        ];
        $routeCreate = route('area.category.create');
        return view('areas.categories.index',
            [
                'datas'=>$datas,
                'routeCreate'=>$routeCreate,
                'queryParams'=>$queryParams,
                'breadcrum'=>$this->breadcrum,
                'title'=>trans('labelAdmin.areasCategory.listTitle'),
            ]);
    }

    public function show($id) {
        $data = $this->modelController->find($id);
        if(!$data) redirect(route('area.category'));
        $title = trans('labelAdmin.areasCategory.detail').' '.$data->title;
        $this->breadcrum.= '<li class="breadcrumb-item active">'.$title.'</li>';
        return view('areas.categories.show',['data' => $data, 'breadcrum'=>$this->breadcrum, 'title'=> $title]);
    }

    public function create(){
        $this->breadcrum.= '<li class="breadcrumb-item active">'.trans('labelAdmin.areasCategory.add').'</li>';
        return view('areas.categories.create',['breadcrum'=>$this->breadcrum, 'title'=> trans('labelAdmin.areasCategory.add')]);
    }

    public function store(AreaCategoryRequest $request){
        $data = $request->all();
        $data['active'] = (!empty($data['active'])) ? 1 : 0;
        $result = $this->modelController->create($data);
        if($result) return redirect()->route('area.category')->with('success','Add new successfully');
    }

    public function edit($id){
        $data = $this->modelController->find($id);
        $title = trans('labelAdmin.areasCategory.edit').' '.$data->title;
        if(!$data) redirect(route('area.category'));
        $this->breadcrum.= '<li class="breadcrumb-item active">'.trans('labelAdmin.areasCategory.edit').$data->title.'</li>';
        return view('areas.categories.edit',['data' => $data,'breadcrum'=>$this->breadcrum,'id'=>$id, 'title'=> $title]);
    }

    public function update(AreaCategoryRequest $request, $id){
        $data = $this->modelController->find($id);
        $data->title = $request->get('title');
        $data->active = (!empty($request->active)) ? 1 : 0;
        $result = $data->update();
        if($result) return redirect()->route('area.category')->with('success','Updated successfully');
    }

    public function destroy($id){
        $this->modelController->find($id)->delete();
        return redirect()->route('area.category')->with('success','Delete successfully');
    }
}