@extends('layouts.app')

@section('content')
    <form action="{!! route('area.category.store') !!}" method="POST" class="form-horizontal form-label-left">
        {{ csrf_field() }}
        @include('areas.categories.fields')
        <button type="submit" class="btn btn-primary">{{ trans('button.submit') }}</button>
    </form>
@endsection