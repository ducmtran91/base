@extends('layouts.app')

@section('content')
    {!! Form::open(['action' => ['LanguagesController@store'], 'method' => 'POST', 'class' => 'form-horizontal form-label-left', 'enctype' => 'multipart/form-data']) !!}
    @include('languages.fields')
    {!! Form::submit(trans('button.submit'), array('class' => 'btn btn-primary')) !!}
    {!! Form::close() !!}
@endsection
