<?php
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App{
/**
 * App\AreaCategory
 *
 * @property int $id
 * @property int $active
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AreaCategory whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AreaCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AreaCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AreaCategory whereUpdatedAt($value)
 */
	class AreaCategory extends \Eloquent {}
}

namespace App{
/**
 * App\Languages
 *
 * @property int $id
 * @property string $title
 * @property string $logo
 * @property string $locale
 * @property int $active
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Languages whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Languages whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Languages whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Languages whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Languages whereLogo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Languages whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Languages whereUpdatedAt($value)
 */
	class Languages extends \Eloquent {}
}

namespace App{
/**
 * App\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string|null $remember_token
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 */
	class User extends \Eloquent {}
}

