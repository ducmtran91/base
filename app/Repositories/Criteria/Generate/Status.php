<?php
/**
 * Created by PhpStorm.
 * User: ductm
 * Date: 7/2/2018
 * Time: 9:09 AM
 */

namespace App\Repositories\Criteria\Generate;


use App\Repositories\Criteria\Criteria;
use App\Repositories\Eloquent\Repository;

class Status extends Criteria
{
    private $status;
    public function __construct($status)
    {
        $this->status = $status;
    }

    public function apply($model, Repository $repository)
    {
        $model = $model->where('active', $this->status);
        return $model;
    }
}