<?php
/**
 * Created by PhpStorm.
 * User: ductm
 * Date: 6/29/2018
 * Time: 2:17 PM
 */
return [
    "status" => [
        0 => "Inactive",
        1 => "Active"
    ],
    "limit" => [
        10 => 10,
        25 => 25,
        50 => 50,
        75 => 75,
        100 => 100,
    ]
];