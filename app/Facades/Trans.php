<?php
/**
 * Created by PhpStorm.
 * User: ductm
 * Date: 7/17/2018
 * Time: 6:21 PM
 */

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class Trans extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'App\Helpers\Trans';
    }
}