<?php
/**
 * Created by PhpStorm.
 * User: ductm
 * Date: 7/12/2018
 * Time: 11:31 AM
 */

namespace App\Http\Controllers\Transform;


class DataLoader
{
    public function __construct()
    {

    }

    public function parseFields($datas, $fields){
        $data = [];
        foreach($datas as $item) {
            if($item) {
                $dataItem = [];
                foreach ($item as $key => $value) {
                    $dataItem = array_merge($dataItem,[$fields[$key] => trim(htmlentities($value, ENT_QUOTES, "UTF-8"))]);
                }
                $data[] = $dataItem;
            }
        }
        return $data;
    }

    public function insertData($dataReader, $header, $model) {
        unset($dataReader[0]);
        $data = $this->parseFields($dataReader, $header);
        foreach($data as $item){
            $dataCheck = $model::where('locale',$item['locale'])->first();
            if($dataCheck){
                $model::where('id',$dataCheck->id)->update($item);
            }else{
                $model::create($item);
            }
        }
    }
}