<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LanguagesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method())
        {
            case 'POST':
                return [
                    'title' => 'required|unique:languages|max:255',
                    'locale' => 'required|unique:languages|max:2',
                ];
                break;
            case 'PUT':
                return [
                    'title' => 'required|max:255|unique:languages,title,' .$this->route()->parameter('id'),
                    'locale' => 'required|max:2|unique:languages,locale,' .$this->route()->parameter('id'),
                ];
                break;
        }
    }
}
