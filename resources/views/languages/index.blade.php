@extends('layouts.app')

@section('content')
    <div id="datatable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
        @include('areas.categories.action')
        @include('languages.filter')
        <div class="row">
            <div class="col-sm-12">
                <table id="datatable" class="table table-bordered">
                    <thead>
                    <tr role="row">
                        <th class="w-stt">#</th>
                        <th>{{ trans('labelAdmin.image') }}</th>
                        <th>{{ trans('labelAdmin.areasCategory.title') }}</th>
                        <th>{{ trans('labelAdmin.areasCategory.status') }}</th>
                        <th class="w-action">{{ trans('labelAdmin.areasCategory.action') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if($datas)
                        @foreach($datas as $item)
                            <tr role="row" class="even">
                                <th scope="row">{!! $item->id !!}</th>
                                <td>
                                    @if($item->logo)
                                    {{ Html::image(url(config('filepath.language_image_show') . $item->logo), $item->title, array( 'width' => 80 )) }}
                                    @endif
                                </td>
                                <td>{!! $item->title !!}</td>
                                <td>{!! $status[$item->active] !!}</td>
                                <td>
                                    {!! Html::decode(link_to_route('language.show', '<i class="fa fa-folder"></i>', ['id' => $item->id], ['class' => 'btn btn-primary btn-xs'])) !!}
                                    {!! Html::decode(link_to_route('language.edit', '<i class="fa fa-pencil"></i>', ['id' => $item->id], ['class' => 'btn btn-primary btn-xs'])) !!}
                                    {!! Form::open(['route' => ['language.destroy', $item->id], 'method' => 'DELETE', 'id' => 'delete']) !!}
                                        {!! Form::token() !!}
                                        {{ Form::button('<i class="fa fa-trash-o"></i>', ['class' => 'btn btn-danger btn-xs', 'type' => 'submit']) }}
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="text-center">
                    {{ $datas->appends($queryParams)->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection
