<?php
return [
    'table_names' => [
        'area_categories' => 'area_categories',
        'area_category_translations' => 'area_category_translations',
        'area_posts' => 'area_posts',
        'area_post_translations' => 'area_post_translations',
        'area_post_details' => 'area_post_details',
        'area_post_detail_translations' => 'area_post_detail_translations',
        'languages' => 'languages',
    ]
];