@push('styles')
    {!! Html::style(asset('libraries/jasny-bootstrap/css/jasny-bootstrap.min.css')) !!}
@endpush
<div class="item form-group">
    @php
        $classFileInput = 'fileinput-new';
        if(!empty($data->logo) || !empty($oldPhoto)){
            $classFileInput = 'fileinput-exists';
        }else{
        }
    @endphp
    <div class="fileinput {!! $classFileInput !!}" data-provides="fileinput">
        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">

        </div>
        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
            @if(!empty($data->logo) || !empty($oldPhoto))
                @php
                    if(!empty($data->logo)){
                        $src = url(config('filepath.language_image_show') . $data->logo);
                    }else{
                        $src = $oldPhoto;
                    }
                @endphp
                <img src="{{ $src }}">
                {!! Html::image($src) !!}
                {!! Form::hidden("image_tmp", $src) !!}
            @endif
        </div>
        <div>
            <span class="btn btn-default btn-file">
                <span class="fileinput-new">Select image</span>
                <span class="fileinput-exists">Change</span>
                <input type="file" name="image" accept="image/*">
            </span>
            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
        </div>
    </div>
</div>
<div class="item form-group">
    {!! Html::decode(Form::label('name', trans('labelAdmin.languages.title').'<span class="text-danger">*</span>', array('class'=> 'control-label col-md-3 col-sm-3 col-xs-12'))) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::text('title', !empty($data) ? $data->title : old('title'), array('class'=> 'form-control', 'id'=> 'title', 'placeholder' => trans('labelAdmin.languages.placeholderTitle')) ) !!}
    </div>
</div>

<div class="item form-group">
    {!! Html::decode(Form::label('name', trans('labelAdmin.languages.locale').'<span class="text-danger">*</span>', array('class'=> 'control-label col-md-3 col-sm-3 col-xs-12'))) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::text('locale', !empty($data) ? $data->locale : old('locale'), array('class'=> 'form-control', 'id'=> 'locale', 'placeholder' => trans('labelAdmin.languages.placeholderLocale')) ) !!}
    </div>
</div>

<div class="item form-group">
    {!! Html::decode(Form::label('name', trans('labelAdmin.languages.status').'<span class="text-danger">*</span>', array('class'=> 'control-label col-md-3 col-sm-3 col-xs-12'))) !!}
    <div class="col-md-9 col-sm-9 col-xs-12 m-t-5">
        {!! Form::checkbox('active', 1, (!empty($data) && $data->active) || old('active') ? true : false, array('class'=> 'js-switch', 'id'=> 'title' )) !!}
    </div>
</div>
@push('scripts')
    {!! Html::script(asset('libraries/jasny-bootstrap/js/jasny-bootstrap.min.js')) !!}
    <script>
        // Override init_InputMask in file resources/assets/js/admin/custom.min.js to prevent mask input for jasny fileinput
        // @link http://www.jasny.net/bootstrap/javascript/#fileinput
        function init_InputMask() {}
    </script>
@endpush
