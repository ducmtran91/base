<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAllDatabaseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tableNames = config('table.table_names');

        Schema::create($tableNames['area_categories'], function(Blueprint $table){
            $table->increments('id');
            $table->boolean('active');
            $table->timestamps();
        });

        Schema::create($tableNames['area_category_translations'], function (Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('area_category_id');
            $table->string('locale',2)->index();
            $table->string('title');
            $table->unique(['area_category_id','locale']);
            $table->foreign('area_category_id')->references('id')->on('area_categories')->onDelete('cascade');
        });

        Schema::create($tableNames['area_posts'], function (Blueprint $table){
            $table->increments('id');
            $table->boolean('active');
            $table->unsignedInteger('area_category_id');
            $table->foreign('area_category_id')->references('id')->on('area_categories')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create($tableNames['area_post_translations'], function(Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('area_post_id');
            $table->string('title');
            $table->string('excerpt');
            $table->string('locale', 2)->index();
            $table->unique(['area_post_id', 'locale']);
            $table->foreign('area_post_id')->references('id')->on('area_posts')->onDelete('cascade');
        });

        Schema::create($tableNames['area_post_details'], function (Blueprint $table){
            $table->increments('id');
            $table->tinyInteger('position');
            $table->unsignedInteger('area_post_id');
            $table->foreign('area_post_id')->references('id')->on('area_posts')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create($tableNames['area_post_detail_translations'], function(Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('content');
            $table->string('locale',2);
            $table->unsignedInteger('area_post_detail_id');
            $table->unique(['area_post_detail_id', 'locale']);
            $table->foreign('area_post_detail_id')->references('id')->on('area_post_details')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $tableNames = config('table.table_names');

        Schema::drop($tableNames['area_categories']);
        Schema::drop($tableNames['area_category_translations']);
        Schema::drop($tableNames['area_posts']);
        Schema::drop($tableNames['area_post_translations']);
        Schema::drop($tableNames['area_post_details']);
        Schema::drop($tableNames['area_post_detail_translations']);
    }
}
