<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
//Error
Route::get('/category', 'AreaCategoryController@index')->name('area.category');
Route::get('/category/create', 'AreaCategoryController@create')->name('area.category.create');
Route::get('/category/{id}', 'AreaCategoryController@show')->name('area.category.show');
Route::get('/category/{id}/edit', 'AreaCategoryController@edit')->name('area.category.edit');
Route::post('/category', 'AreaCategoryController@store')->name('area.category.store');
Route::put('/category/{id}', 'AreaCategoryController@update')->name('area.category.update');
Route::delete('/category/{id}', 'AreaCategoryController@destroy')->name('area.category.destroy');

Route::get('/language', 'LanguagesController@index')->name('language');
Route::get('/language/create', 'LanguagesController@create')->name('language.create');
Route::get('/language/{id}', 'LanguagesController@show')->name('language.show');
Route::get('/language/{id}/edit', 'LanguagesController@edit')->name('language.edit');
Route::post('/language', 'LanguagesController@store')->name('language.store');
Route::put('/language/{id}', 'LanguagesController@update')->name('language.update');
Route::delete('/language/{id}', 'LanguagesController@destroy')->name('language.destroy');
Route::post('/language/import', 'LanguagesController@import')->name('language.import');