<?php
/**
 * Created by PhpStorm.
 * User: ductm
 * Date: 6/11/2018
 * Time: 6:11 PM
 */

namespace App\Repositories\Contracts;


interface RepositoryInterface
{
    public function all($columns = array('*'));

    public function paginate($perPage = 15, $columns = array('*'));

    public function create(array $data);

    public function update(array $data, $id);

    public function delete($id);

    public function find($id, $columns = array('*'));

    public function findBy($field, $value, $columns = array('*'));
}