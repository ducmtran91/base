<?php
/**
 * Created by PhpStorm.
 * User: ductm
 * Date: 7/12/2018
 * Time: 9:36 AM
 */

namespace App\Http\Controllers\Reader;


abstract class FileReader
{
    abstract protected function getPathFile();
    abstract public function parseData();
    abstract public function header($data);//get column header
}