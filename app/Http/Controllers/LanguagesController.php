<?php
/**
 * Created by PhpStorm.
 * User: ductm
 * Date: 6/11/2018
 * Time: 5:58 PM
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Reader\CSVReader;
use App\Http\Requests\FileCsvRequest;
use App\Http\Requests\LanguagesRequest;
use App\Languages;
use App\Repositories\Criteria\Generate\Keyword;
use App\Repositories\Criteria\Generate\Status;
use App\Repositories\Models\LanguagesRepository;
use App\Services\Common;
use App\Services\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;


class LanguagesController extends AppBaseController
{
    private $modelController;
    private $commonService;

    public function __construct(LanguagesRepository $model,Common $common)
    {
        parent::__construct();
        $this->commonService = $common;
        $this->modelController = $model;
        $this->breadcrum = "<li class='breadcrumb-item'><a href='".route('language')."'><i class='fa fa-home'></i>  ".trans('labelAdmin.languages.listTitle')."</a></li>";
    }

    public function index(Request $request) {
        $limit = $request->get('filter_limit') ? $request->get('filter_limit') : $this->perPage;
        $status = $request->get('filter_status') === '' ? '' : $request->get('filter_status');
        $keyword = $request->get('filter_keyword') ? $request->get('filter_keyword') : '';
        $model = $this->modelController;
        if($keyword || $status != ''){
            if($keyword) {
                $model = $model->pushCriteria(new Keyword($keyword));
            }
            if($status != '') {
                $model = $model->pushCriteria(new Status($status));
            }
            $datas = $model->paginate($limit);
        }else{
            $datas = $model->paginate($limit);
        }
        $queryParams = [
            'filter_limit' => $limit,
            'filter_status' => $status,
            'filter_keyword' => $keyword
        ];
        return view('languages.index',
            [
                'datas'=>$datas,
                'queryParams'=>$queryParams,
                'breadcrum'=>$this->breadcrum,
                'title'=>trans('labelAdmin.languages.listTitle'),
            ]);
    }

    public function show($id) {
        $data = $this->modelController->find($id);
        if(!$data) redirect(route('language'));
        $title = trans('labelAdmin.languages.detail').' '.$data->title;
        $this->breadcrum.= '<li class="breadcrumb-item active">'.$title.'</li>';
        return view('languages.show',['data' => $data, 'breadcrum'=>$this->breadcrum, 'title'=> $title]);
    }

    public function create(){
        if(Session::hasOldInput('srcImage')) {
            $oldPhoto = Session::getOldInput('srcImage');
        }
        $this->breadcrum.= '<li class="breadcrumb-item active">'.trans('labelAdmin.languages.add').'</li>';
        return view('languages.create',['breadcrum'=>$this->breadcrum, 'title'=> trans('labelAdmin.languages.add'),'oldPhoto' => !empty($oldPhoto) ? $oldPhoto : '']);
    }

    public function store(Request $request){
        $input = $request->all();
        $files = $request->file();
        $toolImage = new Image();
        if($files) {
            $checkImageBase64 = $toolImage->convertImageToBase64($files['image']->getPathname());
            if (empty($checkImageBase64['success'])) {
                flash(\Lang::get('image.'.$checkImageBase64['msg']))->error();
                return redirect(route('language.create'));
            }
        }
        $rules = ['title' => 'required|unique:languages|max:255','locale' => 'required|unique:languages|max:255'];
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            if ($files) {
                $input['srcImage'] = $checkImageBase64['data'];
            }elseif(!empty($input['image_tmp'])){
                $input['srcImage'] = $input['image_tmp'];
            }
            return redirect('language/create')
                ->withErrors($validator)
                ->withInput($input);
        }
        $input['active'] = empty($data['active']) ? 0 : 1;
        $input['logo'] = null;
        if (!empty($files['image'])) {
            $uploadImage = $this->commonService->uploadFile($files['image'], config('filepath.language_image'));
            if (!empty($uploadImage)) {
                $input['logo'] = $uploadImage;
            }
        }elseif(!empty($input['image_tmp'])){
            $image = $toolImage->convertBase64ToImage($input['image_tmp'], config('filepath.language_image'));
            if($image['success']){
                $input['logo'] = $image['data'];
            }else{
                $input['srcImage'] = $input['image_tmp'];
                flash(\Lang::get('image.'.$image['msg']))->error();
                return redirect(route('language.create'))->withInput($input);
            }
        }
        $result = $this->modelController->create($input);
        if($result) return redirect()->route('language')->with('success','Add new successfully');
    }

    public function edit($id){
        $data = $this->modelController->find($id);
        $title = trans('labelAdmin.languages.edit').' '.$data->title;
        if(!$data) redirect(route('language'));
        $this->breadcrum.= '<li class="breadcrumb-item active">'.trans('labelAdmin.languages.edit').$data->title.'</li>';
        return view('languages.edit',['data' => $data,'breadcrum'=>$this->breadcrum,'id'=>$id, 'title'=> $title]);
    }

    public function update(LanguagesRequest $request, $id){
        $data = $this->modelController->find($id);
        $data->title = $request->get('title');
        $data->locale = $request->get('locale');
        $data->active = empty($request->get('active')) ? 0 : 1;
        $files = $request->file();

        $image=$data->logo;
        if (!empty($files['image'])) {
            $uploadImage = $this->commonService->uploadFile($files['image'], config('filepath.language_image'));
            if (!empty($uploadImage)) {
                $image= $uploadImage;
            }
            $this->commonService->removeFile($data->logo, config('filepath.language_image'));
        }
        $data->logo = $image;
        $result = $data->update();
        if($result) return redirect()->route('language')->with('success','Updated successfully');
    }

    public function destroy($id){
        $this->modelController->find($id)->delete();
        return redirect()->route('language')->with('success','Delete successfully');
    }

    public function import(Request $request) {
        $extCSV = [
            'application/vnd.ms-excel',
            'text/plain',
            'text/csv',
            'text/tsv'
        ];
        $reader = null;
        $ext = $request->file('file')->getMimeType();

        if (in_array($ext, $extCSV)) {
            $validator = Validator::make($request->all(), ['file' => 'required|mimetypes:application/vnd.ms-excel,text/plain,text/csv,text/tsv']);
            if($validator->fails()) {
                return redirect('language')->withErrors($validator)->withInput($request->all());
            }
            $reader = new CSVReader($request, 'file');
        }

        $dataReader = $reader->parseData();

        if(count($dataReader) > 1){
            $header = array_map('strtolower', $reader->header($dataReader));
            $dataTransform = new Transform\DataLoader();
            $dataTransform->insertData($dataReader, $header, Languages::class);
        }
        return redirect()->route('language')->with('success','Import successfully');
    }
}
