<?php
/**
 * Created by PhpStorm.
 * User: ductm
 * Date: 6/15/2018
 * Time: 5:28 PM
 */

namespace App\Repositories\Criteria\Contracts;

use App\Repositories\Criteria\Criteria;
/*We also need to extend our Repository class a bit to cover criteria queries*/
interface CriteriaInterface
{
    /**
     * @param bool $status
     * @return $this
     */
    public function skipCriteria($status = true);

    /**
     * @return mixed
     */
    public function getCriteria();

    /**
     * @param Criteria $criteria
     * @return $this
     */
    public function getByCriteria(Criteria $criteria);

    /**
     * @param Criteria $criteria
     * @return $this
     */
    public function pushCriteria(Criteria $criteria);

    /**
     * @return $this
     */
    public function  applyCriteria();
}